package com.sebastianb.calendar.Activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@Entity
public class Activity {

    private @Id @GeneratedValue Long id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Stockholm")
    private LocalDateTime localDateTime;
    private String title;
    private String description;

    Activity() {}

    Activity(LocalDateTime localDateTime, String title, String description) {
        this.localDateTime = localDateTime;
        this.title = title;
        this.description = description;
    }

}
