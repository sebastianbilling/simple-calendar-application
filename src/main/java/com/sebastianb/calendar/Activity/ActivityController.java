package com.sebastianb.calendar.Activity;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api")
class ActivityController {

    private final ActivityRepository activityRepository;

    private final ActivityResourceAssembler activityResourceAssembler;

    ActivityController(ActivityRepository activityRepository, ActivityResourceAssembler activityResourceAssembler) {
        this.activityRepository = activityRepository;
        this.activityResourceAssembler = activityResourceAssembler;
    }

    @GetMapping("/activities")
    Resources<Resource<Activity>> all() {

        List<Resource<Activity>> activities = activityRepository.findAll().stream()
                .map(activityResourceAssembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(activities,
                linkTo(methodOn(ActivityController.class).all()).withSelfRel());
    }

    @PostMapping("/activities")
    ResponseEntity<?> newActivity(@RequestBody Activity newActivity) throws URISyntaxException {

        Resource<Activity> resource = activityResourceAssembler.toResource(newActivity);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @GetMapping("/activities/{id}")
    Resource<Activity> one(@PathVariable Long id) {

        Activity activity = activityRepository.findById(id)
                .orElseThrow(() -> new ActivityNotFoundException(id));

        return activityResourceAssembler.toResource(activity);
    }

    @PutMapping("/activities/{id}")
    ResponseEntity<?> replaceEmployee(@RequestBody Activity newActivity, @PathVariable Long id) throws URISyntaxException {

        Activity updateActivity = activityRepository.findById(id)
                .map(activity -> {
                    activity.setTitle(newActivity.getTitle());
                    activity.setDescription(newActivity.getDescription());
                    return activityRepository.save(activity);
                })
                .orElseGet(() -> {
                    newActivity.setId(id);
                    return activityRepository.save(newActivity);
                });

        Resource<Activity> resource = activityResourceAssembler.toResource(updateActivity);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @DeleteMapping("/activities/{id}")
    ResponseEntity<?> deleteActivity(@PathVariable Long id) {

        activityRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
