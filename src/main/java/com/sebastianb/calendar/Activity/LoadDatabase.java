package com.sebastianb.calendar.Activity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDateTime;


@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(ActivityRepository activityRepository) {

        Activity activity1 = new Activity(LocalDateTime.now(),"Springa", "Med Jörgen" );
        Activity activity2 = new Activity(LocalDateTime.now().plusDays(1), "Gymma", "Med olof");

        return args -> {
            log.info("Preloading: " + activityRepository.save(activity1));
            log.info("Preloading: " + activityRepository.save(activity2));
        };
    }
}
