package com.sebastianb.calendar.Activity;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;



@Component
class ActivityResourceAssembler implements ResourceAssembler<Activity, Resource<Activity>> {

    @Override
    public Resource<Activity> toResource(Activity activity) {

        return new Resource<>(activity,
                linkTo(methodOn(ActivityController.class).one(activity.getId())).withSelfRel(),
                linkTo(methodOn(ActivityController.class).all()).withRel("activities"));
    }
}
