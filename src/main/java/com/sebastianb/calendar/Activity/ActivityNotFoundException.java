package com.sebastianb.calendar.Activity;


class ActivityNotFoundException extends RuntimeException {

    ActivityNotFoundException(Long id) {
        super("Could not find activity: " + id);
    }

}


