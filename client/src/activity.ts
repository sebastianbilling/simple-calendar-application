export class Activity {
  id: number;
  localDateTime: Date;
  title: string;
  description: string;

}
